package com.example.rationalezadatak.net;

import com.example.rationalezadatak.net.model.Album;
import com.example.rationalezadatak.net.model.Photo;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;


public interface ApiEndpoint {

    @GET("/albums")
    Call<List<Album>> getAlbums();

    @GET("/photos")
    Call<List<Photo>> getPhotos();

}
