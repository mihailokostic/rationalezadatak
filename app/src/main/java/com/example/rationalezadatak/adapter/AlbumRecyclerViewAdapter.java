package com.example.rationalezadatak.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rationalezadatak.R;
import com.example.rationalezadatak.net.model.Album;

import java.util.List;

public class AlbumRecyclerViewAdapter extends
        RecyclerView.Adapter<AlbumRecyclerViewAdapter.MyViewHolder> {

    public List<Album> albumList;
    public Context context;
    public OnRecyclerItemClickListener listener;

    public interface OnRecyclerItemClickListener {
        void onRVItemClick(Album album);
    }

    public AlbumRecyclerViewAdapter(List<Album> albumList, OnRecyclerItemClickListener listener) {
        this.albumList = albumList;
        this.listener = listener;
    }


//    public AlbumRecyclerViewAdapter(List<Album> albumList, Context context) {
//        this.albumList = albumList;
//        this.context = context;
//    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tv_title;
        TextView tv_id;
        CardView mCardView;
        View view;

        public MyViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_title = itemView.findViewById(R.id.tv_title_album);
            tv_id = itemView.findViewById(R.id.tv_id_album);
            mCardView = itemView.findViewById(R.id.card_view);
        }

        public void bind(final Album album, final  OnRecyclerItemClickListener listener) {
            tv_title.setText(album.getTitle());
            tv_id.setText(String.valueOf(album.getId()));

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onRVItemClick(album);
                }
            });
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.album_single_item, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.bind(albumList.get(position), listener);

        int currentPosition = position;
        final Album album = albumList.get(currentPosition);
    }


    @Override
    public int getItemCount() {
        return albumList.size();
    }
}
