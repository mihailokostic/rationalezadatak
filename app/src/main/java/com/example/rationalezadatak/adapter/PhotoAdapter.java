package com.example.rationalezadatak.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rationalezadatak.R;
import com.example.rationalezadatak.activities.AlbumDetail;
import com.example.rationalezadatak.net.model.Album;
import com.example.rationalezadatak.net.model.Photo;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.MyViewHolder> {

    public List<Photo> photoList;
    public Context context;
    public OnPhotoRecyclerItemClickListener listener;

    public interface OnPhotoRecyclerItemClickListener {
        void onRVItemClick(Photo photo);
    }

    public PhotoAdapter(List<Photo> photoList, OnPhotoRecyclerItemClickListener listener) {
        this.photoList = photoList;
        this.listener = listener;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        View view;
        CardView mCardView;
        TextView title_photo;
        ImageView thumb;

        MyViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            mCardView = view.findViewById(R.id.card_view_1);
            title_photo = view.findViewById(R.id.tv_title_photo);
            thumb = view.findViewById(R.id.thumb);
        }

        public void bind(final Photo photo, final OnPhotoRecyclerItemClickListener listener) {
            title_photo.setText(photo.getTitle());
            Picasso.get().load(photo.getThumbnailUrl()).into(thumb);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onRVItemClick(photo);
                }
            });
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.album_detail_single_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.bind(photoList.get(position), listener);

        int currentPosition = position;
        final Photo photo = photoList.get(currentPosition);

    }

    @Override
    public int getItemCount() {
        return photoList.size();
    }
}
