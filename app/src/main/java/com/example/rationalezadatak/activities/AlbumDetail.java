package com.example.rationalezadatak.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.rationalezadatak.R;
import com.example.rationalezadatak.adapter.PhotoAdapter;
import com.example.rationalezadatak.net.ApiEndpoint;
import com.example.rationalezadatak.net.Contract;
import com.example.rationalezadatak.net.model.Album;
import com.example.rationalezadatak.net.model.Photo;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AlbumDetail extends AppCompatActivity implements Serializable,
        PhotoAdapter.OnPhotoRecyclerItemClickListener {

    Album album;
    String albumId;
    List<Photo> photoList;
    Photo photo;
    Context context;
    ImageView thumb;

    RecyclerView recyclerView;
    ProgressDialog progressDoalog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_detail);

        takeIntent();

        progressDoalog = new ProgressDialog(AlbumDetail.this);
        progressDoalog.setMessage("Loading....");
        progressDoalog.show();

        ApiEndpoint service = Contract.getRetrofitInstance().create(ApiEndpoint.class);
        Call<List<Photo>> call = service.getPhotos();
        call.enqueue(new Callback<List<Photo>>() {
            @Override
            public void onResponse(Call<List<Photo>> call, Response<List<Photo>> response) {
                progressDoalog.dismiss();
                setupList(response.body());
            }

            @Override
            public void onFailure(Call<List<Photo>> call, Throwable t) {
                progressDoalog.dismiss();
                Toast.makeText(AlbumDetail.this, "Error.. ", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setupList(List<Photo> photoList) {

        recyclerView = findViewById(R.id.recycler_view_photo);
        recyclerView.setLayoutManager(new LinearLayoutManager(AlbumDetail.this));
        recyclerView.setAdapter(new PhotoAdapter(photoList, AlbumDetail.this));
    }

    private void takeIntent() {
        Album album = (Album) getIntent().getSerializableExtra("albumId");
//        photoList = album.getPhotos();
    }

    @Override
    public void onRVItemClick(Photo photo) {
//        Picasso.with(this).load(photo.getLink())
//                .error(R.drawable.placeholder)
//                .placeholder(R.drawable.placeholder)
//                .into(photoImage);

//        Intent intent = new Intent(photo);
//        Picasso.get().load(photo.getUrl()).into(thumb);

            Dialog dialog = new Dialog(AlbumDetail.this);
            dialog.setContentView(R.layout.image_dialog);
            dialog.setTitle("Image dialog");

            ImageView thumb = dialog.findViewById(R.id.imageFullSize);
            Picasso.get().load(photo.getUrl()).into(thumb);

            dialog.show();
    }
}
