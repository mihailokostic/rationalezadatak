package com.example.rationalezadatak.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.rationalezadatak.R;
import com.example.rationalezadatak.adapter.AlbumRecyclerViewAdapter;
import com.example.rationalezadatak.adapter.PhotoAdapter;
import com.example.rationalezadatak.net.ApiEndpoint;
import com.example.rationalezadatak.net.Contract;
import com.example.rationalezadatak.net.model.Album;
import com.example.rationalezadatak.net.model.Photo;

import java.io.Serializable;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements Serializable,
        AlbumRecyclerViewAdapter.OnRecyclerItemClickListener {

    RecyclerView recycler_view;
    ProgressDialog progressDoalog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressDoalog = new ProgressDialog(MainActivity.this);
        progressDoalog.setMessage("Loading....");
        progressDoalog.show();

        ApiEndpoint service = Contract.getRetrofitInstance().create(ApiEndpoint.class);
        Call<List<Album>> call = service.getAlbums();
        call.enqueue(new Callback<List<Album>>() {
            @Override
            public void onResponse(Call<List<Album>> call, Response<List<Album>> response) {
                progressDoalog.dismiss();
                setupList(response.body());
            }

            @Override
            public void onFailure(Call<List<Album>> call, Throwable t) {
                progressDoalog.dismiss();
                Toast.makeText(MainActivity.this, "Error.. ", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setupList(List<Album> albumList) {

        recycler_view = findViewById(R.id.recycler_view);
        recycler_view.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        recycler_view.setAdapter(new AlbumRecyclerViewAdapter(albumList, MainActivity.this));
    }


    @Override
    public void onRVItemClick(Album album) {
        Intent intent = new Intent(MainActivity.this, AlbumDetail.class);
        intent.putExtra("album", album);
        startActivity(intent);
    }

}
